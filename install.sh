#!/bin/sh

ln -sf $PWD/vim/configs/vimrc ~/.vim/vimrc
ln -sf $PWD/vim/configs/gvimrc ~/.vim/gvimrc
ln -sf $PWD/vim/UltiSnips ~/.vim/UltiSnips

ln -sf $PWD/tmux/tmux.conf ~/.tmux.conf
