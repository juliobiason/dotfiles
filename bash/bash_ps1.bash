FORE_ORANGE="\[$(tput setaf 208)\]"
FORE_PURPLE="\[$(tput setaf 103)\]"
FORE_GREEN="\[$(tput setaf 148)\]"
RESET="\[$(tput sgr0)\]"
git_branch() {
	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1/'
}
export PS1="${FORE_BOLD}${FORE_PURPLE}\u${FORE_GREEN}\$(git_branch) ${FORE_ORANGE}\w \$${RESET} "

