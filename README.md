# README #

Welcome to my personal DotFiles repository.

... 'cause all the cool kids have one, right?

So far, the only things being managed here are

* A simple but cool Bash prompt.
* Emacs (tentative)
* i3/sway
* Tmux config
* VIM configs

## The Bash Prompt

The easiest way to install the Bash prompt is to add, on your `.bashrc`:

```bash
source $HOME/<path>/dotfiles/bash/bash_ps1.bash
```

Yup, just this.
